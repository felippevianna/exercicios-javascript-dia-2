function ordenar(arrayNums) {
	if (arrayNums.length <= 1) { 
		return arrayNums;
	} else {

		var ladoEsq = [];
		var ladoDir = [];
		var arrayOrdenado = [];
		var valorComp = arrayNums.pop();
		var tamanhoArray = arrayNums.length;

		for (var i = 0; i < tamanhoArray; i++) {
			if (arrayNums[i] <= valorComp) {
				ladoEsq.push(arrayNums[i]);
			} else {
				ladoDir.push(arrayNums[i]);
			}
		}

		return arrayOrdenado.concat(ordenar(ladoEsq), valorComp, ordenar(ladoDir));
	}
}

function gerarArray(tam) {
    let i = 1
    let numsAleatorios = []
    while (i <= tam){
        let aleatorio = Math.round(Math.random()*100)
        numsAleatorios.push(aleatorio)
        i++
    }
    console.log("Array gerado(sem ordenação): "+ numsAleatorios)
    return ordenar(numsAleatorios)
}


let tamArray = Math.round(Math.random()*10)
let arrayAleatorio = gerarArray(tamArray)
console.log("Array gerado(ordenado): "+ arrayAleatorio)

