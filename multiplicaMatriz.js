const produtoMatrizes = function (matA, matB) {
    if (matA[0].length === matB.length){
        let numLinhas = matA.length
        let numCols = matB[0].length
        let novaMat = new Array(numLinhas)
        for (let i = 0; i < numLinhas; i++){
            novaMat[i] = new Array(numCols)
            for (let j = 0; j< numCols; j++){
                novaMat[i][j] = 0
                for (let k = 0; k < matA[0].length; k++) {
                    novaMat[i][j] += matA[i][k] * matB[k][j]
                }
            }
        }

        console.log(novaMat)

    } else {
        return console.log("Não é possóvel realizar a operação")
    }
}

// Casos de teste
// Caso 1
let matrizA = [[2,-1], [2,0]]
let matrizB = [[2,3], [-2,1]]
produtoMatrizes(matrizA, matrizB)
// Caso 2
let matrizC = [[4,0], [-1,-1]]
let matrizD = [[-1,3], [2,7]]
produtoMatrizes(matrizC, matrizD)
